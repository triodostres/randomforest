#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ^^^^^^^Permite usar acentos e coisas legais

#import plotly.plotly as py
#import plotly.graph_objs as go
#import statistics

def printAccuracy(stats, name, f, b):
    color = f * b
    traces = []
    accs = []
    i = 0
    #Para cada dataframe
    for stat in stats:
        accuracy = 0
        #Conta o numero de verdadeiros positivos
        for col in list(stat):
            accuracy += float(stat.loc[col][col] / stat.sum().sum())
        accs.append(accuracy)
        i += 1
    '''
    red = 5 * (i%color + 1)
    green = int(color%120 / (i+1))
    blue = color%100 + i
    '''
    red = 0
    green = 102
    blue = 204

    box = go.Box(
        y = accs,
        name='#' + str(i) + " trees",
        marker=dict(
            color='rgb(' + str(red) + ', ' + str(green) + ', ' + str(blue) + ')',
        ),
        boxmean=True
    )
    traces.append(box)

    layout = go.Layout(title='Accuracy box plot', width=800, height=640)
    fig = go.Figure(data=traces, layout=layout)

    py.image.save_as(fig, filename='acc-case-' + name + "-fold" + str(f) + "-boot" + str(b) + '.jpeg')

    print "----Accuracy for case " + name
    accuracyFloat = [float(j) for j in accs]
    print "--Mean = " + str(statistics.mean(accuracyFloat))
    print "--Standard Deviation = " + str(statistics.stdev(accuracyFloat))
    print "Box plot of accuracy at <acc-case-" + name + "-fold" + str(f) + "-boot" + str(b) + ".jpeg>"

def printFMeasure(stats, name, f, b):
    color = b * f
    traces = []
    fms = []
    i = 0
    for stat in stats:
        fmeasure = 0
        TP = 0  # Verdadeiro positivo
        TN = 0  # Verdadeiro negativo
        FN = 0  # Falso negativo
        FP = 0  # Falso positivo
        #Conta o numero de verdadeiros positivos
        for col in list(stat):
            TP = stat.loc[col][col]
            FP = stat[col].sum() - TP
            for index, row in stat.iterrows():
                if index == col:
                    FN = row.sum() - TP
            if(TP + FN == 0):
                recall = 0
            else:
                recall = TP / (TP + FN)
            if(TP + FP == 0):
                precision = 0
            else:
                precision = TP / (TP + FP)
            if(recall + precision == 0):
                fmeasure = 0
            else:
                fmeasure += 2 * recall * precision / (recall + precision)
        fmean = fmeasure / len(list(stat))
        fms.append(fmean)
        i += 1

    '''
    red = 10 * (i % color)
    green = int( color % 255 / (i+1) )
    blue = color % 90 + i
    '''
    red = 0
    green = 102
    blue = 204

    box = go.Box(
        y = fms,
        name='#' + str(i) + " trees",
        marker=dict(
            color='rgb(' + str(red) + ', ' + str(green) + ', ' + str(blue) + ')',
        ),
        boxmean=True
    )
    traces.append(box)
    layout = go.Layout(title='F-measure box plot', width=800, height=640)
    fig = go.Figure(data=traces, layout=layout)

    py.image.save_as(fig, filename='fm-case'+ name + "-fold" + str(f) + "-boot" + str(b) +'.jpeg')


    print "----F-Measure"
    fMeasureFloat = [float(j) for j in fms]
    print "--Mean = " + str(statistics.mean(fMeasureFloat))
    print "--Standard Deviation = " + str(statistics.stdev(fMeasureFloat))
    print "Box plot of F-measure at <fm-case-" + name + "-fold" + str(f) + "-boot" + str(b) + ".jpeg>"