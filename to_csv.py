#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ^^^^^^^Permite usar acentos e coisas legais
import sys, getopt
import csv
import pandas as pd

def main(argv):
    inputFile = 'data/wdbc'
    header = []
    data = []
    try:
        opts, args = getopt.getopt(argv, "hi:")
    except getopt.GetoptError:
        print 'python to_csv.py -i <input file>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'python to_csv.py -i <input file>'
            sys.exit()
        elif opt in ("-i"):
            inputFile = arg

    #Header contém o nome de cada coluna separado por ';', sendo que a coluna alvo se chama 'target'
    with open(inputFile+".header", "r") as headerFile:
        line = headerFile.readline()
        line = line[:-1]
        header = line.split(';')

    with open(inputFile+".data", "r") as dataFile:
        reader = csv.reader(dataFile)
        for line in reader:
            data.append(line)

    final = pd.DataFrame(data=data, columns=header)

    if 'ID' in final:
        final = final.set_index('ID')
    if 'Diagnosis' in final:
        final = final.rename(index=str, columns={"Diagnosis": "target"})

    print list(final)
    header = list(final)
    newHeader = []
    print len(header)
    for i in range(0, len(header)):
        if header[i] != 'target':
            newHeader.append(header[i])
    newHeader.append('target')
    final = final.reindex(columns=newHeader)

    with open(inputFile+".csv", "w") as writeFile:
        final.to_csv(writeFile, sep=",")

    writeFile.close()
    dataFile.close()
    headerFile.close()

if __name__ == "__main__":
    main(sys.argv[1:])