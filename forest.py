#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ^^^^^^^Permite usar acentos e coisas legais
import dataProc as proc
import sys, getopt
import tree
import graphics as gp

def main(argv):
    #Valores default -> só mudam se for chamado com valores diferentes no prompt
    inputFile = "data/wine"
    stats = []
    #Número de folds
    f = 10
    #Número de árvores
    b = 1
    #Valor da seed
    seed = 2

    #Pega inputs do prompt, se for chamado por lá
    try:
        opts, args = getopt.getopt(argv, "hi:b:s:f:")
    except getopt.GetoptError:
        print 'python forest.py -i <input file> -b <# of bootstraps> -f <# of folders> -s <value of seed>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'python forest.py -i <input file> -b <# of bootstraps> -f <# of folders> -s <value of seed>'
            sys.exit()
        elif opt in ("-i"):
            inputFile = arg
        elif opt in ("-b"):
            b = int(arg)
        elif opt in ("-f"):
            f = int(arg)
        elif opt in ("-s"):
            seed = int(arg)

    #Primeiro processamento de dados: para dataframe e folds
    data, types = proc.getData(inputFile)
    folds = proc.kfold(f, inputFile, seed)
    #Zera o f pro print ficar mais bonito  :)
    f = 0
    for fold in folds:
        #print fold
        '''
        #para cada fold, repete o processo
        fold = fold.drop(fold.columns[0], axis=1) #Tira index
        for iteration in range(0, b):
            train, test = proc.bootstrap(fold, seed + iteration)
            print "----------------------------BUILD Tree "+str(iteration)+"(FOLD: "+str(f)+") ------------------------------"
            root, heads = tree.buildTree(types,train, seed)
            root.printAll(0)
            print "-----------------------------TEST Tree "+str(iteration) +"(FOLD: "+str(f)+")------------------------------"
            #Lista de dataframes
            #NICOLAS DEBUGANDO
            stats.append(tree.testTree(test, root, data))
        f += 1
        '''

    names = inputFile.split('/')
    #gp.printAccuracy(stats, names[-1], f, b)
    #gp.printFMeasure(stats, names[-1], f, b)

if __name__ == "__main__":
    main(sys.argv[1:])
