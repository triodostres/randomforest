# RandomForest

---

##Objetivos

O grupo deve implementar:

   - O algoritmo de indução de uma árvore de decisão, usando como critério de seleção de atributos para divisão de nós o Ganho de Informação (baseado no conceito de entropia), como visto na disciplina, tratando tanto atributos categóricos quanto numéricos;

   - Uma função para percorrer a árvore de decisão treinada e realizar a classificação de uma nova instância (do conjunto de teste);

   - O mecanismo de bootstrap (amostragem com reposição) para geração de subconjuntos a partir do conjunto de dados de treinamento originais. Cada bootstrap será utilizado como conjunto de treinamento de um modelo/árvore no aprendizado __ensemble__;

   - O mecanismo de amostragem de __m__ atributos a cada divisão de nó, a partir dos quais serão será selecionado o melhor atributo de acordo com o Ganho de Informação;

   - O treinamento de um __ensemble__ de árvores de decisão, adotando os mecanismos de bootstrap e seleção de atributos com amostragem, como mencionados acima;

   - O mecanismo de votação majoritária entre as múltiplas árvores de decisão no __ensemble__, para classificação de novas instâncias utilizando o modelo de Florestas Aleatórias;

   - A técnica de validação cruzada (__cross-validation__) estratificada, para avaliar poder de generalização do modelo e a variação de desempenho de acordo com diferentes valores para os parâmetros do algoritmo (p.ex., número de árvores no __ensemble__).

---

##Detalhes de implementação

A divisão de nós no algoritmo básico de indução de árvores de decisão deve ser tratada tanto
para **atributos categóricos** quanto **atributos numéricos**. Para **atributos numéricos**, sugere-se utilizar
como valor de divisão a **média aritmética** entre os valores do atributo na partição analisada ou a
média calculada de acordo com a estratégia vista em aula (ver Aula 03). Os grupos podem assumir
que o tipo de cada atributo será informado manualmente junto com o __dataset__ para classificação.

Cada grupo, ao aplicar sua implementação de Florestas Aleatórias nos problemas de classificação,
deverá utilizar a metodologia de validação cruzada estratificada (__cross-validation__, fazendo uso de conjuntos de treino e teste), a fim de avaliar o desempenho do modelo e o efeito de diferentes
valores de parâmetros no aprendizado do algoritmo. Especificamente, o parâmetro a ser otimizado
neste trabalho é **número de árvores** no __ensemble__ (ntree), tal que a implementação deve ser
executada para diferentes número de árvores no __ensemble__ (ex: 10, 25, 50,...), onde a escolha do limite
superior dependerá de custos computacionais envolvidos na execução do algoritmo, e os respectivos
desempenhos deverão ser comparados.  O **número de atributos amostrados** (m), pode ser
otimizado ou pode ser adotado o valor padrão sugerido na literatura, i.e., raiz quadrada do número
total de atributos no __dataset__, a critério do grupo.

---

##Entrega e relatório

A entrega será via Moodle. É necessário haver:
* Código-fonte e relatório

* O relatório deverá estar em formato pdf , e deverá conter uma descrição das características gerais da implementação de cada grupo (p.ex., descrição das estruturas de dados utilizadas para armazenar as árvores, como é feita a classificação de novas instâncias, detalhes sobre possíveis otimizações feitas para tornar o algoritmo mais eficiente, etc), análise da corretude da implementação, e análise de desempenho do algoritmo implementados para diferentes valores de __ntree__ em __datasets__ selecionados (ver detalhes abaixo);

* Cada grupo deverá apresentar em seu relatório a árvore induzida por sua implementação para um conjunto de dados de benchmark , fornecido pelos professores juntamente com a enunciado do trabalho. O objetivo deste teste é permitir com que os professores verifiquem a corretude da implementação básica de cada grupo. O conjunto de dados de  benchmark  está disponível  no Moodle da disciplina. Espera-se que os grupos apresentem tanto a estrutura final da árvore induzida, como os valores de Ganho de Informação de cada divisão de nós realizada.  Ao garantir a corretude da implementação do algoritmo de indução de árvores de decisão, os grupos poderão ter maior confiança no funcionamento do algoritmo de Florestas Aleatórias implementado.

* Após verificada a corretude do algoritmo de indução de uma árvore de decisão, como descrito acima, cada grupo deverá treinar o seu algoritmo de Florestas Aleatórias (e avaliar a sua performance com __cross-validation__) nos seguintes __datasets__ básicos de classificação:

      1. [Pima Indian Diabetes Data Set](https://github.com/EpistasisLab/penn-ml-benchmarks/tree/master/datasets/classification/pima) (8 atributos, 768 exemplos, 2 classes) **Objetivo**: predizer se um paciente tem diabetes a partir de um pequeno conjunto de dados clínicos.

      2. [Wine Data Set](https://archive.ics.uci.edu/ml/datasets/wine) (13 atributos, 178 exemplos, 3 classes) **Objetivo**: predizer o tipo de um vinho baseado em sua composição química.

      3. [Ionosphere Data Set](https://archive.ics.uci.edu/ml/datasets/Ionosphere) (34 atributos, 351 exemplos, 2 classes) **Objetivo**: predizer se a captura de sinais de um radar da ionosfera é adequada para análises posteriores ou não (’good’ ou ’bad’).

      4. (opcional; pontos extras) [Breast Cancer Wisconsin](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic)) (32 atributos, 569 exemplos, 2 classes) **Objetivo**: predizer se um determinado exame médico indica ou não a presença de câncer.

* Para cada __dataset__ citado acima, utilize o método de validação cruzada estratificada para treinar e testar o algoritmo de Florestas Aleatórias. O objetivo é comparar a performance média de modelos de Floresta Aleatória para diferentes valores do parâmetro ntree.

* Sugere-se utilizar __k = 10__ folds na validação cruzada. Destes __k__ folds, __k − 1__ folds serão usados para construção do modelo de Florestas Aleatórias com  ntree árvores, aplicando-se o algoritmo de indução de árvores de decisão combinado aos processos de amostragem de instâncias (bootstrap, i.e., amostragem com reposição) e de atributos (com base no parâmetro m). O fold restante (de teste) será usado para avaliar o desempenho do __ensemble__. Isto é, em cada etapa da validação cruzada, será gerada não apenas uma árvore de decisão, mas múltiplos modelos de árvore de decisão (tantos quantos forem o valor de ntree), os quais irão compor o modelo de Florestas Aleatórias. Perceba que como o treinamento de cada árvore envolve aleatoriedade, haverá diversidade entre as árvores geradas a partir do mesmo conjunto de __k − 1__ folds de treinamento.  Este processo de treinamento e teste será repetido __k__ vezes, de acordo com o funcionamento básico da estratégia de validação cruzada visto em aula. O uso de validação cruzada repetida é opcional.

* Como medida de performance utilizada neste trabalho, indicamos o uso da **F1-measure**, definida em função da precisão e do recall (atribuindo mesmo peso a ambas, isto é, __β = 1__).

* Mostre em seu relatório, através de gráficos, o desempenho do algoritmo conforme a variação no parâmetro __ntree__. De acordo com a teoria sobre aprendizado __ensemble__, espera-se diminuição no erro de classificação de acordo com o aumento no número de árvores no modelo de Florestas Aleatórias.

**Dica**: Tendo em vista a aleatoriedade envolvida no treinamento do algoritmo de Florestas Aleatórias, recomenda-se utilizar o processo de inicialização manual de
seeds de números aleatórios a fim de permitir a replicação dos resultados no processo de debugging do código.