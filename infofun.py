#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ^^^^^^^Permite usar acentos e coisas legais
import math
import dataProc as proc

#Cálculo de InfoD, de acordo com a apresentação "aula03", slide 54
def InfoD(total, classes): #Classes eh coluna de dataframe
    somatorio = 0.0
    for index, row in classes.iterrows():
        classe = float(row)
        if classe != 0:
            somatorio += (classe / total) * math.log((classe / total), 2)
        return -somatorio
		
#Cálculo de InfoDA, de acordo com a apresentação "aula03", slide 56
def InfoDA(data): #Matriz eh um dataframe
    total = data.values.sum()
    somatorio = 0.0
    for col in data:
        if(type(data[col]) != 'pandas.core.dataframe.Dataframe'):
            item = data[col].to_frame()
        else:
            item = data[col]
        somatorio += data[col].sum()/total * InfoD(data[col].sum(), item)
    return somatorio

#Cálculo de Gain, de acordo com a apresentação "aula03", slide 57
def Gain(data, attribute):
    header = list(data)	#Atributos
    total = data.shape[0] #Número de linhas
    classes = data[header[-1]].value_counts().to_frame() #Pega o número de cada classe-alvo
    parsed = proc.getRelMatrix(data, attribute, header[-1])
    gain = InfoD(total, classes) - InfoDA(parsed)
    return gain

#Com base nas funções acima, busca pelo ganho máximo
def findMaxGain(data):
    gains = []
    header = list(data)
    targets = len(list(data[header[-1]].unique()))  #Num de classes alvo
    for attribute in data:
        if attribute is not header[-1]:
            gain = Gain(data, attribute)
            gains.append(gain)
    #Dados todos os ganhos, escolhe o index do que maximiza (max(gains))
    maxGain = gains.index(max(gains))
    attr = header[maxGain]
    #retorna o nome do atributo e o valor do ganho
    return attr, max(gains)