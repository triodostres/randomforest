#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ^^^^^^^Permite usar acentos e coisas legais
import pandas as pd
import numpy as np
import math
import random

#Usa um csv (contendo os dados em forma de dataframe - header + index + dados)
#e um txt que diz se o atributo (incluindo o target) é "continuo" ou "discreto"
def getData(input):
    with open(input+'.csv', 'rb') as readFile:
        data = pd.read_csv(readFile, sep=',')
    with open(input+'.txt', 'r') as typeFile:
        line = typeFile.readline()
        line = line[:-1] #Tira o \n final
        types = line.split(';')
    return data, types #retorna um dataframe e o tipo de cada atributo

#Pega os pontos de divisao, sendo eles:
#Valores discretos, se a coluna é do tipo "discreto"
#Média (único valor), se a coluna é do tipo "continuo"
def getSplitPoints(data, types):
    splits = [] #lista de tuplas ou médias
    header = list(data) #Pega a primeira linha do dataframe -> header
    for col in header[:-1]:
        #Se o tipo do atributo atual é contínuo...
        if types[header.index(col)] == "continuo":
            somatorio = data[col].sum()
            media = somatorio / data.shape[0]
            #Adiciona a média na lista (single split point)
            splits.append(media)
        #Se é do tipo discreto...
        else:
            #Escolhe os valores "únicos" da coluna (tipos diferentes)
            values = data[col].unique()
            values = tuple(values)
            #Retorna uma tupla de valores únicos
            splits.append(values)
    return splits

#Retorna todas as linhas (num dataframe) cuja coluna tenha atributo igual a "attribute"
#Tipo: data, coluna 0, "x"
#D1 - x   y   z	   -> retorna
#D2 - a   y   w	   -> não retorna
#D3 - x   b   j    -> retorna
def splitData(data, col, attribute):
    return data.loc[data[col] == attribute]

#Pega o dataframe e, dado um atributo (col), cria uma matriz de relação (apresentação "aula03", slide 58)
def getRelMatrix(data, col, target):
    #Colunas -> tipos diferentes de valores para o atributo "col"
    values = list(data[col].unique()) 
    #Linhas -> tipos diferentes de classes-alvo para o atributo "target"
    targets = list(data[target].unique())
    row = 0
    column = 0
    #Inicializa uma matriz com linhas = número de classes target e colunas = número de valores possíveis para o atributo
    matrix = np.zeros((len(targets), len(values)))
    #Cria um dataframe que é, basicamente, a matriz de relação (conta as combinações entre o valor de col e target)
    df = data.groupby([col, target]).count()
    for value in values:
        row = 0
        for targ in targets:
            #para cada target, associa um valor de atributo a uma classe-alvo
            tupla = (value, targ)
            #Se a combinação estiver no dataframe (número > 0), cria uma célula com o valor da combinação
            if tupla in df.index:
                if df.shape[1] == 0:
                    cell = 1
                else:
                    cell = df.ix[tupla,0]
                #Se a combinação não existir, atribui o valor 0 à célula da nova matriz (melhorada)
            else:
                cell = 0
            #Adiciona a célula à matriz de relação, onde row representa a classe-alvo e column, o atributo
            matrix[row, column] = cell
            row += 1
        column += 1
    #Transforma a matriz num dataframe, usando as classes-alvo como index e os valores do atributo como colunas
    final = pd.DataFrame(data=matrix, index = targets, columns = values)
    return final

#Verifica se o dataframe passado contém apenas uma classe-alvo
def isCompleted(data):
    newData = data
    #Pega o header do dataframe
    header = list(data)
    #Se só tiver uma coluna, adiciona um dummy --- dá um outro erro
    if data.shape[1] == 1:
        data['dummy'] = data[header[0]]
    #Cria um dataframe que contém a contagem apenas das classes-alvo
    #Nota o header[-1]? É por isso que o target tem que estar na última coluna...
    df = data.groupby([header[-1]]).count()
    #Se o número de linhas é 1
    if df.shape[0] == 1: #a classe é única
        classe = df.index.values[0]
        #Sim, a classe é única: retorna a classe
        return True, classe
    #caso contrário, procura a maior classe
    else:
        classes = []
        nums = []
        #print df
        #Itera pelos atributos
        for value in df.index.values:
            classes.append(value)
            #Conta o número de valores que correspondem àquele atributo
            nums.append(df.ix[value, 0])
        #Pega a classe (De classes) que tem o maior número (max(nums)) de instâncias
        maxClass = classes[nums.index(max(nums))]
        #Não, a classe não é a única: retorna a que maximiza
        return False, maxClass

def getMaxClass(data):
    #Pega o header do dataframe
    header = list(data)
    #Cria um dataframe que contém a contagem apenas das classes-alvo
    df = data.groupby([header[-1]]).count()
    classes = []
    nums = []
    #Itera pelos atributos
    for value in df.index.values:
        classes.append(value)
        #Conta o número de valores que correspondem àquele atributo
        nums.append(df.ix[value, 0])
    #Pega a classe (De classes) que tem o maior número (max(nums)) de instâncias
    maxClass = classes[nums.index(max(nums))]
    #Não, a classe não é a única: retorna a que maximiza
    return maxClass


#K-foldzão da massa: que nem o trabalho anterior, na versão função
def kfold(k, inputFile, seed):
    classes = []    #ISSO AQUI É UMA LISTA COM OS TARGETS POSSIVEIS, TIPO ASSIM  [benigno, maligno]
    nclasses = []   #ISSO AQUI É O NUMERO DE INSTANCIAS QUE TEM AQUELA CLASSE, TIPO ASSIM [52, 70] significa que tem 52 benigno e 70 maligno
    pclasses = []   #ISSO AQUI É TIPO O DE CIMA, SÓ QUE É UM PERCENTUAL DO TOTAL
    folds = []
    instances = []
    toWrite = []
    final = []
    resto = []

    data, types = getData(inputFile)
    if (k == 1):
        folds.append(data)
        return folds
    header = list(data)
    df = data.groupby([header[-1]]).count()
    for value in df.index.values:
        nclasses.append(df.ix[value, 0])
        classes.append(value)
    for value in nclasses:
        pclasses.append(float(value) / float(sum(nclasses)))

    inst_per_fold = int(sum(nclasses) / k)
    extra_inst = int(sum(nclasses) % k)

   # print "\n\n\n\n hueheuheuheu clases: \n", classes, "\n\n\n"
   # print "\n\n\n\n hueheuheuheu Nclases: \n", nclasses, "\n\n\n"
    #print "\n\n\n\n hueheuheuheu Pclases: \n", pclasses, "\n\n\n"



    # Transforma em percentual
    for target in classes:
        folds.append(data[data[header[-1]] == target])

    #print "\n\n\n\n hueheuheuheu folds: \n", folds, "\n\n\n"

    for katchau in range(1, k + 1):
        #print "\n\n hueheuheuheu XABLAU: \n", katchau, "\n"
        if extra_inst != 0:
            extra_inst -= 1
            extra = 1
        else:
            extra = 0
        #print "\nextra: ",extra,"\n"
        instances_total = inst_per_fold + extra
        del toWrite[:]
        del instances[:] #conserto magico do stefaniak obg obg louvado seje
        for i in range(0, len(classes)):
            #print "\n\n hueheuheuheu JJJJJJJquenaverdadeéI: \n", i, "\n"
            #print "\n\n hueheuheuheu fold \n ", folds[i], "\n"
            #if(i == 1):
                #print "\nint loco q da probs no 2: ",int(pclasses[i] * instances_total)," == ",pclasses[i] * instances_total,"\n"
            instances.append(int(pclasses[i] * instances_total))
            '''
            LAUREN NAO NOS BATE, ESTAMOS DEBUGANDO VORAZMENTE E ESSE IF PARECE ERRADO
            if i == len(classes) - 1:
                print "\n\n hueheuheuheu numero loko mais loki: \n", instances_total - sum(instances), "\n"
                instances.append(instances_total - sum(instances))
            else:
                print "\n\n hueheuheuheu numero loko1: \n", pclasses[i] * instances_total, "\n"
                print "\n\n hueheuheuheu numero loko2: \n", int(pclasses[i] * instances_total), "\n"
                instances.append(int(pclasses[i] * instances_total))
            '''

            # Calcular os folds
            #Com Seed gera sempre os mesmos folds -- ", random_state = 2"
            toWrite.append(folds[i].sample(n=instances[i]))
            #toWrite.append(folds[i].sample(n=instances[i], random_state = seed))     #random state é pra manter a SEED igual a cada execuçaõ
            folds[i] = folds[i].drop(toWrite[-1].index.values)   #TESTE DO NICOLAS FEAT STEFANIAK, isso aqui remove o que ja foi usado
            #print "\n\n hueheuheuheu twouraiti: \n", toWrite, "\n"
        #print "\n\n hueheuheuheu instances total: \n", instances_total, "\n"
        #print "\n\n hueheuheuheu sum instances: \n", sum(instances), "\n"
        #print "\n\n hueheuheuheu twouraiti numero: ", katchau, "\n", toWrite, "\n"
        write = pd.concat(toWrite)
        #write = write.sample(frac=1) #1 fold
        final.append(write)
    for c in final:
        data = data.drop(c.index.values)    #pega oq nao entrou nos folds por arredondamento
    #print "\nnewdata : ",data,"\n"
    p=0
    #print "First data: \n",data,"\n"
    while(data.empty==False):   #a ideia aqui eh fazer um roundrobin nos folds colocando uma por uma as linhas q sobraram no final
        #print "\n",data.drop(data.head(data.shape[0]-1).index),"\n"
        final[p].append(data.drop(data.head(data.shape[0]-1).index))
        data = data.drop(data.tail(1).index)
        p+=1
        if(p==k):
            p=0
    #print "\nnewnewdata : ", data, "\n"
    return final
	
#Retorna o training e o testing dataframe
def bootstrap(data, seed):
    #Training: pega uma amostra de tamanho máximo (frac=1) e aceita duplos (replace=True)
    #Com o Seed gera sempre os mesmos bootstraps -- ", random_state = 2"
    #training = data.sample(frac=1, replace=True, random_state = seed)    #random state é pra manter a SEED igual a cada execução
    training = data.sample(frac=1, replace=True)
    #Testing: faz a "diferença" entre o dataframe completo e o de treino
    testing = data.drop(training.index.values)
    return training, testing

#Seleciona a sqrt(m), onde m é o número de atributos
def attributeSelection(data, types, seed):
    newTypes = []
    cols = list(data)
    number = int(math.sqrt(len(cols)))
    random.seed(a=seed)
    header = random.sample(cols[:-1], number)
    target = data[['target']]

    data = data.drop(['target'], axis=1)

    #Com o Seed gera sempre os mesmos atributos
    final = data[header]    #random state é pra manter a SEED igual a cada execuçaõ
    final = pd.concat([final, target], axis=1)

    #Para cada atributo do subdataset
    for item in header:
        #Pega o  index no dataset original
        index = cols.index(item)
        #Pega os tipos correspondentes
        newTypes.append(types[index])

    return final, newTypes