#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ^^^^^^^Permite usar acentos e coisas legais
import os
import sys, getopt

def main(argv):
    inputFile = "data/wine"
    seed = 2
    try:
        opts, args = getopt.getopt(argv, "hi:s:")
    except getopt.GetoptError:
        print 'python script.py -i <input file>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'python script.py -i <input file>'
            sys.exit()
        elif opt in ("-i"):
            inputFile = arg
    print "--Starting process for " + inputFile

    '''
    #iscripiti bunitu da lauren   :)
    fold = 1
    for i in range(10):
        boot = 2
        for j in range (10):
            print "---Process started with " + str(boot) + " bootstraps and " + str(fold) + " folds"
            os.system( "python forest.py -i " + inputFile + " -b " + str(boot) + " -f " + str(fold) + " -s " + str(seed) )
            boot += 2
        fold += 1
    '''

    #iscripiti feiu do nicolas   >:(
    fold = 10

    boot = 10
    print "---Process started with " + str(boot) + " bootstraps and " + str(fold) + " folds"
    os.system("python forest.py -i " + inputFile + " -b " + str(boot) + " -f " + str(fold) + " -s " + str(seed))
    boot = 25
    print "---Process started with " + str(boot) + " bootstraps and " + str(fold) + " folds"
    os.system("python forest.py -i " + inputFile + " -b " + str(boot) + " -f " + str(fold) + " -s " + str(seed))
    boot = 50
    print "---Process started with " + str(boot) + " bootstraps and " + str(fold) + " folds"
    os.system("python forest.py -i " + inputFile + " -b " + str(boot) + " -f " + str(fold) + " -s " + str(seed))
    boot = 100
    print "---Process started with " + str(boot) + " bootstraps and " + str(fold) + " folds"
    os.system("python forest.py -i " + inputFile + " -b " + str(boot) + " -f " + str(fold) + " -s " + str(seed))
 
    print "--Process completed."

if __name__=='__main__':
   main(sys.argv[1:])
