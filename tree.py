#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ^^^^^^^Permite usar acentos e coisas legais
import sys
import infofun as fun
import dataProc as proc
import numpy as np
import pandas as pd

class Node(object):
    def __init__(self, data, col, type):
        # Caso seja um teste, recebe uma lista de atributos(discreto) ou valor de corte (continuo)
        # Caso seja uma folha, recebe a classe alvo
        self.data = data
        self.col = col
        self.type = type #"teste" ou "folha"
        self.child = []

    def add_gain(self, gain): #para fins de debug
        self.gain = gain

    def add_child(self, node):
        self.child.append(node)
	
    #Testa o tipo de divisão do nó
    def test(self, attribute):
        if type(self.data) == tuple: #discreto -> um filho para cada valor
            for item in self.data:
                if attribute == item:
                    return self.child[self.child.index(item)]
        if type(self.data) == float: #continuo -> ou é menor ou é maior
            if attribute < self.data:
                return self.child[0]
            else:
                return self.child[1]
        print 'Attribute not found'
        sys.exit()

    #Imprime o nodo de acordo com o seu tipo
    def printNode(self, i):
        for x in range(0, i):
            sys.stdout.write('|\t')
        if self.type == "leaf":
            print "-Class = " + str(self.data)
        elif type(self.data) == tuple: #discreto
            print "-Gain {} -- Test {} -- Choose between: {}".format(self.gain, self.col, self.data)
        else:
            print "-Gain {} -- Test {} -- IF data < {} GOTO children[0], ELSE GOTO children[1]".format(self.gain, self.col, self.data)

    #Imprime o nó e os filhos -- como se o nó fosse a raiz de uma subárvore
    def printAll(self, i):
        self.printNode(i)
        for child in self.child:
            child.printAll(i+1)

#Fnção maneira para construir árvore. 
#Recebe uma lista de tipos: "continuo" ou "discreto"
def buildTree(types, data, seed):
    #Se tem mais de uma coluna (target + atributo)
    #e uma ou mais instâncias (folha não é none)
    if data.shape[1] >= 2 and data.shape[0] >= 1:
        #testar se chegou numa classe só
        fin, classe = proc.isCompleted(data) #Por algum motivo, altera data
        if 'dummy' in data.columns: #Oioioi gambiarra
            data = data.drop(['dummy'], axis=1)
        if(fin): #achou uma folha
            leaf = Node(classe, 'none', 'leaf')
            return leaf, list(data)
        else: #Caso não tenha chegado ao fim, é nó de teste
            #seleciona os novos atributos
            header = list(data) #Todos os atributos de data
            subData, subTypes = proc.attributeSelection(data, types, seed)
            #Pega o maior ganho dados os atributos do dataframe
            attr, maxGain = fun.findMaxGain(subData)
            #Usa o index do atributo e pega os pontos de divisão
            subIndex = list(subData).index(attr)
            index = header.index(attr)
            splits = proc.getSplitPoints(subData, subTypes) #subset
            #cria um nó de teste e adiciona -- postumamente -- o ganho
            root = Node(splits[subIndex], list(subData)[subIndex], 'test') #subset
            root.add_gain(maxGain)
            #Pega todos os atributos do dataframe -- EXCETO o que dá maior ganho -- e cria os filhos
            newTypes = [x for i,x in enumerate(types) if i!=index] #data -- index para o header geral
            #newTypes = types
            #Se o tipo do atributo que divide o nó atual é discreto (tem uma lista de valores)...
            if type(splits[subIndex]) == 'list':
                for item in splits[subIndex]:
                    #Cria um filho para cada valor
                    newData = data[data[attr] == item]
                    newData = newData.drop(labels=attr, axis=1)

                    #PARTE QUE O NICOLAS FEZ, NAO CONFIE PIAMENTE NISSO, ESTEJAM AVISADOS
                    if newData.empty:
                        #se algum item nao tiver NENHUMA instancia, entao volta tuto: o nó atual nao é mais um teste,
                        # é uma folha da classe com mais ocorrencias
                        #precisamos encontrar qual a classe com mais ocorrencias (ver dataProc -> isCompleted)
                        classeMaioria = proc.getMaxClass(data)
                        root = Node(classeMaioria, 'none', 'leaf')
                        return root, list(data)

                    node, headChi = buildTree(newTypes, newData, seed)
                    root.add_child(node)
                    header = set(header) & set(headChi)
                #Se for do tipo "continuo"...
            else:
                #Cria um filho (child[0]) para valores menores que a média
                les = data[data[attr] < splits[subIndex]]
                les = les.drop(labels=attr, axis=1)
                # PARTE QUE O NICOLAS FEZ, NAO CONFIE PIAMENTE NISSO, ESTEJAM AVISADOS

                #Cria um filho (child[1]) para valores maiores ou iguais à média
                ge = data[data[attr] >= splits[subIndex]]
                ge = ge.drop(labels=attr, axis=1)

                # PARTE QUE O NICOLAS FEZ, NAO CONFIE PIAMENTE NISSO, ESTEJAM AVISADOS
                if (les.empty) or (ge.empty):
                    # se algum dos filhos nao tiver NENHUMA instancia, entao volta tuto: o nó atual nao é mais um
                    # teste, é uma folha da classe com mais ocorrencias
                    # precisamos encontrar qual a classe com mais ocorrencias (ver dataProc -> isCompleted)
                    classeMaioria = proc.getMaxClass(data)
                    root = Node(classeMaioria, 'none', 'leaf')
                    return root, list(data)

                nodeLes, headLes = buildTree(newTypes, les, seed)
                root.add_child(nodeLes)

                #Pega os atributos que já foram usados e dropa eles
                #Próximo filho terá menos possibilidades
                nodeGe, headGe = buildTree(newTypes, ge, seed)
                root.add_child(nodeGe)
                #Pega atributos que sobraram (não foram usados)
                header = set(headGe) & set(header) & set(headLes)
            return root, header

    elif data.shape[0] > 0: #eh folha, com certeza
        #Terminar - pegar classe majoritaria
        fin, classe = proc.isCompleted(data)
        leaf = Node(classe, 'none', 'leaf')
        return leaf, list(data)

    else: #eh uma folha vazia...TEORICAMENTE NUNCA CHEGA NISSO AQUI...teoricamente...
        dummy = Node('none', 'none', 'leaf')
        return dummy, list(data)


#Teste -- ou classificação de uma nova instância
def testTree(data, tree, dataoriginal):
    stats = []
    header = list(data)
    #NICOLAS DEBUGANDO, se usar "data" dá erros aleatorios dependendo do fold
    unique = list(dataoriginal[header[-1]].unique())
    conf_matrix = np.zeros((len(unique), len(unique)))
    header = list(data)
    print "----------------BEGIN------------------"
    #Para cada linha do dataframe de teste...
    for index, row in data.iterrows():
        #testa a instância
        ans = testInstance(row, tree)
        #Se a resposta for igual ao esperado...
        #print ('Index #%d - Predicted: %d\tExpected: %d\n' %(index, ans, row[header[-1]]))
        print "Index #",index, " - Predicted: ", ans, "\tExpected: ", row[header[-1]], "\n"   #esse print funciona pra strings
        if row[header[-1]] == ans:
            stats.append(1)
            #Adiciona ao VP mais 1
            #print np.matrix(conf_matrix)
            conf_matrix[unique.index(ans)][unique.index(ans)] += 1
        elif ans!= 'none':
            stats.append(0)
            #Na linha REAL e na coluna ESPERADA, adiciona 1
            #print np.matrix(conf_matrix)
            conf_matrix[unique.index(ans)][unique.index(row[header[-1]])] += 1
    print "Mean = " + str(float(sum(stats)) / len(stats))
    print "-----------------END-------------------"
    #Retorna uma lista de acertos e erros. Basta fazer os cálculos em cima disso.
    dataFrame = pd.DataFrame(data=conf_matrix, index=unique, columns=unique)
    return dataFrame

def testInstance(instance, tree):
    #Se eh um teste
    if tree.type == "test":
        #E eh multipla escolha
        if type(tree.data) == tuple:
            if instance[tree.col] in tree.data:
                #Continua testando os filhos
                index_child = tree.data.index(instance[tree.col])
                return testInstance(instance, tree.child[index_child])
            #Se é continuo
        else:
            #Se é menor que a média, testa o child[0]
            if instance[tree.col] < tree.data:
                return testInstance(instance, tree.child[0])
            #Se é maior ou igual à média, testa o child[0]
            else:
                return testInstance(instance, tree.child[1])
    #Se é uma folha, basta retornar o dado (classe-alvo)
    else:
        return tree.data